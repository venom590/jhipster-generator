Dieses Projekt beinhaltet einen Generator, welcher ein UML-Diagramm in jHipster kompatible JDL-Dateien umwandelt.
Die hier zu findenen Dateien stellen ein komplettes Eclipse Projekt dar, welches direkt in eine passende Umgebung
eingef�gt werden kann. Dazu muss folgendes beachtet werden:
1) Eclipse Mars.1 Release (4.5.1) runterladen
2) Eclipse �ffnen -> Help -> Install Modelling Components
3) XPand und Papyrus installieren und Eclipse neu starten
4) File -> Import -> General -> Existing Projects into Workspace
5) Select root directory: ausw�hlen -> Browse -> das Projekt ausw�hlen

Anschlie�end befinden sich im Projekt: "generator_demo" unter "src/main/resources/de/ul/generator"
und dann unter /xpand/main.xpt der Code, welcher durch den hierarchischen Aufbau des UML-Diagrams
passende ".jh"-Datei erstellt. Diese befinden sich im Ordner "Workspace\generator_demo\generated".
Sollte der "Run-Button" nicht funktionieren, so befindet sich unter "src/main/resources/de/ul/generator/wf/"
eine "main.mwe" und diese dann via Rechtsklick -> Run As -> MWE Workflow starten.

Um die erzeugten ".jh"-Dateien zu verwenden muss zun�chst jHipster installiert werden, dazu bitte
den Anweisungen unter http://jhipster.github.io/jhipster-uml/ folgen.
Anschlie�end k�nnen neue Dateien wie folgend im Terminal eingebunden werden.
"jhipster-uml <your_file.jh> [-db (sql | mongodb | cassandra)]"


Das hier dargestellte UML-Diagramm zeigt eine Beispiel Tierklinik des Model2Roo-Projects.  https://model2roo.googlecode.com/files/PetClinic.UML.zip
Der grundlegende Code wurde von Herrn Schmidt, Mitarbeiter der Universit�t Leipzig, zur verf�gung gestellt. http://bis.informatik.uni-leipzig.de/JohannesSchmidt